package dev.kowaluk.tramapp.client.sip

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SipTramApiInterface {
    @GET("/api/GetStops")
    fun getAllStops(@Query("userCode") userCode: String, @Query("userApiKey") userApiKey: String) : Call<List<SipStop>>

    @GET("/api/GetLatestPanelPredictions")
    fun getStopDepartures(@Query("userCode") userCode: String, @Query("userApiKey")userApiKey: String, @Query("stopId") stopId: String) : Call<List<SipStopDeparture>>


    companion object {
        const val SERVER_IP: String = "https://public-sip-api.tw.waw.pl"
        const val USER_CODE = "WWW"
        const val USER_API_KEY = "3aAhqA2/*RWsmvy}P8AsxgtFZ"

        fun create() : SipTramApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(Gson().newBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create()))
                .baseUrl(SERVER_IP)
                .build()
            return retrofit.create(SipTramApiInterface::class.java)
        }
    }
}