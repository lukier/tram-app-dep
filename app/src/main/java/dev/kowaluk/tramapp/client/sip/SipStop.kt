package dev.kowaluk.tramapp.client.sip

data class SipStop(
        val stopId: String,
        val name: String,
        val direction: String,
        val gpsX: Double,
        val gpsY: Double,)
