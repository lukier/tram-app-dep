package dev.kowaluk.tramapp.client

data class Stop(val stopId: String, val name: String, val direction: String)