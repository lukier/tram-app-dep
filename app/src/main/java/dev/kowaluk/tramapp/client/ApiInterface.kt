package dev.kowaluk.tramapp.client

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("stops")
    fun getStops(@Query("lat") lat: Double, @Query("lng") lng: Double) : Call<List<Stop>>

    @GET("stops/{id}")
    fun getStopDepartures(@Path("id")stopId: String) : Call<List<StopDeparture>>

    companion object {
        const val SERVER_IP: String = "http://192.168.0.234:8080/"

        fun create() : ApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_IP)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}