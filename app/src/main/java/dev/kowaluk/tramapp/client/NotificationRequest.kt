package dev.kowaluk.tramapp.client

data class NotificationRequest(
    val userId: String,
    val line: String,
    val stopId: String,
    val minutesBefore: Int
)
