package dev.kowaluk.tramapp.client.sip

data class SipStopDeparture(
    val id: Int,
    val line: String,
    val sideNumber: String,
    val stopId: String,
    val destination: String,
    val arrival: Int,
    val fromSchedule: Boolean,
    val lowFloor: Boolean,
    val onStop: Boolean,
    val disruption: Boolean,
)
