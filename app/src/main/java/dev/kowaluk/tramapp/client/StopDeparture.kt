package dev.kowaluk.tramapp.client

data class StopDeparture(val line: String, val destination:String, val arrival: Int, val arrivalAccurate: String)