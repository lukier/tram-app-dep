package dev.kowaluk.tramapp.notification

import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.IBinder
import dev.kowaluk.tramapp.R
import dev.kowaluk.tramapp.client.sip.SipStopDeparture
import dev.kowaluk.tramapp.client.sip.SipTramApiInterface
import retrofit2.Call
import retrofit2.Response
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit


class DepartureNotificationService : Service() {
    val scheduler = Executors.newScheduledThreadPool(1);
    val api = SipTramApiInterface.create()
    var notification: Notification? = null
    var lineValue: String? = null
    var minutesBefore: Int? = null
    var waitForDeparture = true
    var schedulerObserver : ScheduledFuture<*>? = null
    var notify  : ScheduledFuture<*>? = null
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        lineValue = intent?.getStringExtra("line")
        minutesBefore =  intent?.getStringExtra("beforeMinutes")?.toInt()
        notification = getNotification("Poinformuj $minutesBefore minut przed odjazdem dla lini $lineValue");
        startForeground(ONGOING_NOTIFICATION_ID, notification)
        val runner: () -> Unit = {
            notificationCheckRunner()
        }
        schedulerObserver =  scheduler.scheduleAtFixedRate(runner, 5, 20, TimeUnit.SECONDS)
        return START_NOT_STICKY
    }

    fun getNotification(contentText: String): Notification {
        return Notification.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.departure_notification)
            .setContentText(contentText)
            .setContentTitle("Informuj o odjeździe")
            .setAutoCancel(true)
            .build();
    }

    fun notificationCheckRunner()  {
        api.getStopDepartures(SipTramApiInterface.USER_CODE, SipTramApiInterface.USER_API_KEY, "201003")
            .enqueue(object : retrofit2.Callback<List<SipStopDeparture>> {
                override fun onResponse(call: Call<List<SipStopDeparture>>, response: Response<List<SipStopDeparture>>) {

                    val departure = response.body()!!
                        .find {
                            it.line == lineValue && it.arrival == minutesBefore
                        }
                    if (departure != null) {
                        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE)
                        if ( notificationManager is NotificationManager) {
                            notificationManager.notify(ONGOING_NOTIFICATION_ID, getNotification("Masz odjazd!"))
                        }
                    }
                    schedulerObserver?.cancel(true)

                }
                override fun onFailure(call: Call<List<SipStopDeparture>>, t: Throwable) {
                }
            })
    }

    companion object {
        const val ACTION_STOP = "ACTION_STOP"
        const val ONGOING_NOTIFICATION_ID = 123
        const val CHANNEL_ID = "Channel id"
    }
}