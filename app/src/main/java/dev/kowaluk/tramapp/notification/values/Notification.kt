package dev.kowaluk.tramapp.notification.values

data class Notification(
    val line: String,
    val stopId: Int,
    val minutesBefore: Int
)