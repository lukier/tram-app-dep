package dev.kowaluk.tramapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.recyclerview.widget.RecyclerView
import dev.kowaluk.tramapp.client.sip.SipStop
import dev.kowaluk.tramapp.client.sip.SipStopDeparture
import dev.kowaluk.tramapp.client.sip.SipTramApiInterface
import dev.kowaluk.tramapp.departuresView.DepartureAdapter
import dev.kowaluk.tramapp.notification.DepartureNotificationService
import retrofit2.Call
import retrofit2.Response
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


class MainActivity : AppCompatActivity() {

    private val scheduler = Executors.newScheduledThreadPool(1);
    private val api = SipTramApiInterface.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        runDepartureListener()

        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener {
            val editTextMinutesBefore = findViewById<EditText>(R.id.edit_text_minutes_before)
            val lineValue = findViewById<Spinner>(R.id.line_spinner).selectedItem.toString()
            val minutesBefore = editTextMinutesBefore.text.toString()

            editTextMinutesBefore.onEditorAction(EditorInfo.IME_ACTION_DONE)

            println("Linia: $lineValue Minuta: $minutesBefore")
            val intent = Intent(this, DepartureNotificationService::class.java).also { intent ->
                intent.putExtra("line", lineValue)
                intent.putExtra("beforeMinutes", minutesBefore)
                startService(intent)
            }
        }

        createChannel()

        //val lineValue = findViewById<Spinner>(R.id.line_spinner).selectedItem.toString()

    }

    fun createChannel() {
        // Create the NotificationChannel
        val name = "Notyfikacje Tramwaje"
        val descriptionText = "Notyfikacje o przyjeździe tramwaju"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val mChannel = NotificationChannel(DepartureNotificationService.CHANNEL_ID, name, importance)
        mChannel.description = descriptionText
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(mChannel)


    }

    private fun runDepartureListener() {
        val departureAdapter = DepartureAdapter()
        val recyclerView: RecyclerView = findViewById(R.id.lv_departures)
        recyclerView.adapter = departureAdapter


        val depRunner: () -> Unit = {
            api.getStopDepartures(SipTramApiInterface.USER_CODE, SipTramApiInterface.USER_API_KEY, "201003")
                .enqueue(object : retrofit2.Callback<List<SipStopDeparture>> {
                    override fun onResponse(call: Call<List<SipStopDeparture>>, response: Response<List<SipStopDeparture>>) {
                        println("Odpowiedz odjazdy: " + response.body())
                        var lines = response.body()?.map { it.line }?.distinct()?.toList()
                        appendToLines(lines!!)
                        departureAdapter.submitList(response.body() as MutableList<SipStopDeparture>)
                    }
                    override fun onFailure(call: Call<List<SipStopDeparture>>, t: Throwable) {
                        println("Blad przy pobieraniu odjazdów: " + t.message)
                    }
                })
        }

        scheduler.scheduleAtFixedRate(depRunner, 0, 60, TimeUnit.SECONDS)
    }

    fun appendToLines(lines: List<String>) {
        val lineSelector = findViewById<Spinner>(R.id.line_spinner)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, lines)
        lineSelector.adapter = adapter;
    }
    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        fun distFrom(lat1: Double, lng1: Double, lat2: Double, lng2: Double) : Double {
            val earthRadius = 6371000.0 //meters
            val dLat = Math.toRadians(lat2 - lat1);
            val dLng = Math.toRadians(lng2 - lng1);
            val a = sin(dLat / 2) * sin(dLat / 2) +
                    cos(Math.toRadians(lat1)) * cos(Math.toRadians(lat2)) *
                    sin(dLng / 2) * sin(dLng / 2)
            val c = 2 * atan2(sqrt(a), sqrt(1 - a))
            return earthRadius * c
        }
    }
}