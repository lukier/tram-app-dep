package dev.kowaluk.tramapp.departuresView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.kowaluk.tramapp.R
import dev.kowaluk.tramapp.client.sip.SipStopDeparture

class DepartureAdapter () :
    ListAdapter<SipStopDeparture, DepartureAdapter.DepartureViewHolder>(DepartureDiffCallback) {
        class DepartureViewHolder(itemView: View) :
            RecyclerView.ViewHolder(itemView) {
            private val lineNumberView: TextView = itemView.findViewById(R.id.line_text)
            private val destinationTextView: TextView = itemView.findViewById(R.id.destination_text)
            private val timeView: TextView = itemView.findViewById(R.id.time_text)

            init {

            }
            fun bind(departure: SipStopDeparture) {
                lineNumberView.text = departure.line
                destinationTextView.text = departure.destination
                timeView.text = departure.arrival.toString() + " min"
            }
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartureViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.destination_item_layout, parent, false)

        return DepartureViewHolder(view);
    }

    override fun onBindViewHolder(holder: DepartureViewHolder, position: Int) {
        val destination = getItem(position)
        holder.bind(destination)
    }
}

object DepartureDiffCallback : DiffUtil.ItemCallback<SipStopDeparture>() {
    override fun areItemsTheSame(oldItem: SipStopDeparture, newItem: SipStopDeparture): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: SipStopDeparture, newItem: SipStopDeparture): Boolean {
        return oldItem.id == newItem.id
    }
}